import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LectorDeArchivosCSV {

    private static final String SEPARATOR = ",";

    private List<Alumno> alumnos;

    public LectorDeArchivosCSV() {
        alumnos = new ArrayList<>();
    }

    public List<Alumno> leerAlumnosDeArchivoCSV(String fileName) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            boolean esPrimeraLinea = true;
            while (null != line) {
                String[] fields = line.split(SEPARATOR);
                if (!esPrimeraLinea) {
                    Alumno a = new Alumno(fields[1], fields[2], Integer.parseInt(fields[0]));

                    this.alumnos.add(a);
                }
                esPrimeraLinea = false;

                line = br.readLine();
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return alumnos;
    }


}
