import java.util.ArrayList;
import java.util.List;

public class DigitalHouseManager {


    private List<Alumno> alumnos;
    private List<Profesor> profesores;
    private List<Curso> cursos;
    private List<Inscripcion> inscripciones;

    public DigitalHouseManager(){
        this.alumnos = new ArrayList<>();
        this.profesores = new ArrayList<>();
        this.cursos = new ArrayList<>();
        this.inscripciones = new ArrayList<>();
    }

    public void altaCurso(String nombre, Integer codigoCurso, Integer cupoMaximoDealumnos){

        Curso curso = new Curso(nombre,codigoCurso,cupoMaximoDealumnos);

        this.cursos.add(curso);

    }

    public void bajaCurso(Integer codigoCurso){

        for (int i = 0; i < cursos.size(); i++){
            if(this.cursos.get(i).getCodigoCurso().equals(codigoCurso)){
                this.cursos.remove(i);
            }
        }

    }


    public void altaProfesorAdjunto(String nombre, String apellido, Integer codigoProfesor, Integer
            cantidadDeHoras){

        ProfesorAdjunto profesor = new ProfesorAdjunto(nombre,apellido,codigoProfesor,cantidadDeHoras);
        this.profesores.add(profesor);
    }

    public void altaProfesorTitular(String nombre, String apellido, Integer codigoProfesor,
                                    String especialidad){

        ProfesorTitular profesor = new ProfesorTitular(nombre,apellido,codigoProfesor,especialidad);
        this.profesores.add(profesor);

    }

    public void bajaProfesor(Integer codigoProfesor){
        for (int i = 0 ; i < profesores.size(); i++){
            if(profesores.get(i).getCodigoProfesor().equals(codigoProfesor)){
                this.profesores.remove(i);
            }
        }
    }

    public void altaAlumno(String nombre, String apellido, Integer codigoAlumno){
        Alumno alumno = new Alumno(nombre,apellido,codigoAlumno);

        this.alumnos.add(alumno);

    }

    public void inscribirAlumno(Integer codigoAlumno, Integer codigoCurso){
        Alumno alumno = buscarAlumno(codigoAlumno);
        Curso curso = buscarCurso(codigoCurso);

        if(curso != null && alumno != null){

            if (curso.agregarUnAlumno(alumno)){
                Inscripcion inscripcion = new Inscripcion(alumno,curso);

                this.inscripciones.add(inscripcion);
                System.out.println("La inscripción del alumno "+ alumno.toString() + " al curso " + curso.toString() + " se realizó correctamente.");
            }else {
                System.out.println("La inscripción del alumno  al curso no se realizó porque no hay cupo disponible.");

            }

        }


    }

    private Curso buscarCurso(Integer codigoCurso) {
        for (int i=0;i< cursos.size(); i++){
            if(cursos.get(i).getCodigoCurso().equals(codigoCurso)){
                return cursos.get(i);
            }
        }
        return null;
    }

    private Alumno buscarAlumno(Integer codigoAlumno) {

        for (int i=0;i< alumnos.size(); i++){
            if(alumnos.get(i).getCodigoAlumno().equals(codigoAlumno)){
                return alumnos.get(i);
            }
        }
        return null;
    }

    public void asignarProfesores(Integer codigoCurso, Integer codigoProfesorTitular, Integer
            codigoProfesorAdjunto){

        ProfesorTitular profesorTitular = buscarProfesorTitular(codigoProfesorTitular);
        ProfesorAdjunto profesorAdjunto = buscarProfesorAdjunto(codigoProfesorAdjunto);

        Curso curso = buscarCurso(codigoCurso);
        curso.setProfesorAdjunto(profesorAdjunto);
        curso.setProfesorTitular(profesorTitular);


    }

    private ProfesorTitular buscarProfesorTitular(Integer codigoProfesorTitular) {

        for (int i=0;i< profesores.size(); i++){
            if(profesores.get(i).getCodigoProfesor().equals(codigoProfesorTitular)){

                return (ProfesorTitular) profesores.get(i);
            }
        }
        return null;

    }

    private ProfesorAdjunto buscarProfesorAdjunto(Integer codigoProfesorAdjunto) {

        for (int i=0;i< profesores.size(); i++){
            if(profesores.get(i).getCodigoProfesor().equals(codigoProfesorAdjunto)){

                return (ProfesorAdjunto) profesores.get(i);
            }
        }
        return null;

    }

    public List<Alumno> getAlumnos() {
        return alumnos;
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public List<Inscripcion> getInscripciones() {
        return inscripciones;
    }

    public void inscribirListaAlumno(List<Alumno> alumnos, int codigoCurso) {

       for (int i = 0 ; i < alumnos.size(); i++){
           inscribirAlumno(alumnos.get(i).getCodigoAlumno(),codigoCurso);
       }

    }

    public void altaListaAlumnos(List<Alumno> alumnos) {

        for (int i = 0 ; i < alumnos.size(); i++){

            altaAlumno(alumnos.get(i).getNombre(),alumnos.get(i).getApellido(),alumnos.get(i).getCodigoAlumno());
        }
    }
}
