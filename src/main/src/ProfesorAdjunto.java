public class ProfesorAdjunto extends Profesor {


    private Integer cantidadHorasConsultas;

    public ProfesorAdjunto(String nombre, String apellido, Integer codigoProfesor,Integer cantidadHorasConsultas) {
        super(nombre, apellido, codigoProfesor);

        this.cantidadHorasConsultas = cantidadHorasConsultas;
    }

    public Integer getCantidadHorasConsultas() {
        return cantidadHorasConsultas;
    }

    public void setCantidadHorasConsultas(Integer cantidadHorasConsultas) {
        this.cantidadHorasConsultas = cantidadHorasConsultas;

    }



}
