import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Curso {

    private String nombre;
    private Integer codigoCurso;
    private Integer cupoMaximo;
    private List<Alumno> inscriptos;
    private Profesor profesorTitular;
    private Profesor profesorAdjunto;


    public Curso(String nombre, Integer codigoCurso, Integer cupoMaximo) {
        this.nombre = nombre;
        this.codigoCurso = codigoCurso;
        this.cupoMaximo = cupoMaximo;
        this.inscriptos = new ArrayList<>();
    }

    public Boolean agregarUnAlumno(Alumno unAlumno){

        if ((getCupoMaximo() - this.inscriptos.size())  > 0){
            inscriptos.add(unAlumno);
            return true;
        }
        return false;
    }

    public void eliminarAlumno(Alumno unAlumno){
        this.inscriptos.remove(unAlumno);
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigoCurso() {
        return codigoCurso;
    }

    public Integer getCupoMaximo() {
        return cupoMaximo;
    }

    public void setCupoMaximo(Integer cupoMaximo) {
        this.cupoMaximo = cupoMaximo;
    }

    public List<Alumno> getInscriptos() {
        return inscriptos;
    }

    public Profesor getProfesorTitular() {
        return profesorTitular;
    }

    public void setProfesorTitular(Profesor profesorTitular) {
        this.profesorTitular = profesorTitular;
    }

    public Profesor getProfesorAdjunto() {
        return profesorAdjunto;
    }

    public void setProfesorAdjunto(Profesor profesorAdjunto) {
        this.profesorAdjunto = profesorAdjunto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Curso curso = (Curso) o;
        return Objects.equals(codigoCurso, curso.codigoCurso);
    }

    @Override
    public String toString() {
        return "Curso{" +
                "nombre='" + nombre + '\'' +
                ", codigoCurso=" + codigoCurso +
                '}';
    }
}
