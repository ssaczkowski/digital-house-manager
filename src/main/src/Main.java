import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        ProfesorTitular profesorTitular1 = new ProfesorTitular("Eduardo", "Parra", 0, "Programación");
        ProfesorTitular profesorTitular2 = new ProfesorTitular("Patricio", "Ugarte", 1, "Programación");


        ProfesorAdjunto profesorAdjunto1 = new ProfesorAdjunto("Eduardo", "Parra", 2,5);
        ProfesorAdjunto profesorAdjunto2 = new ProfesorAdjunto("Patricio", "Ugarte", 3,5);


        Curso curso1 = new Curso("Full Stack", 20001, 3);
        Curso curso2 = new Curso("Android", 20002, 2);

        curso1.setProfesorTitular(profesorTitular1);
        curso1.setProfesorAdjunto(profesorAdjunto1);


        curso2.setProfesorTitular(profesorTitular2);
        curso2.setProfesorAdjunto(profesorAdjunto2);

        Alumno alumno1 = new Alumno("Pedro", "Martinez", 0);
        Alumno alumno2 = new Alumno("Maria", "Gonzalez", 1);
        Alumno alumno3 = new Alumno("Oscar", "Medina", 2);

        DigitalHouseManager digitalHouseManager = new DigitalHouseManager();
        digitalHouseManager.altaProfesorAdjunto(profesorAdjunto1.getNombre(), profesorAdjunto1.getApellido(), profesorAdjunto1.getCodigoProfesor(), profesorAdjunto1.getCantidadHorasConsultas());
        digitalHouseManager.altaProfesorAdjunto(profesorAdjunto2.getNombre(), profesorAdjunto2.getApellido(), profesorAdjunto2.getCodigoProfesor(), profesorAdjunto2.getCantidadHorasConsultas());

        digitalHouseManager.altaProfesorTitular(profesorTitular1.getNombre(), profesorTitular1.getApellido(), profesorTitular1.getCodigoProfesor(), profesorTitular1.getEspecialidad());
        digitalHouseManager.altaProfesorTitular(profesorTitular2.getNombre(), profesorTitular2.getApellido(), profesorTitular2.getCodigoProfesor(), profesorTitular2.getEspecialidad());


        digitalHouseManager.altaAlumno(alumno1.getNombre(), alumno1.getApellido(), alumno1.getCodigoAlumno());
        digitalHouseManager.altaAlumno(alumno2.getNombre(), alumno2.getApellido(), alumno2.getCodigoAlumno());
        digitalHouseManager.altaAlumno(alumno3.getNombre(), alumno3.getApellido(), alumno3.getCodigoAlumno());


        //Se observa que la firma dada no contempla el caso de que un curso tenga asignado un profesor. Por lo tanto se pierde el registro.
        digitalHouseManager.altaCurso(curso1.getNombre(), curso1.getCodigoCurso(), curso1.getCupoMaximo());
        digitalHouseManager.altaCurso(curso2.getNombre(), curso2.getCodigoCurso(), curso2.getCupoMaximo());



        digitalHouseManager.inscribirAlumno(0, 20001);

        digitalHouseManager.inscribirAlumno(1, 20001);

        digitalHouseManager.inscribirAlumno(0, 20002);
        digitalHouseManager.inscribirAlumno(1, 20002);
        digitalHouseManager.inscribirAlumno(2, 20002);

        digitalHouseManager.bajaProfesor(profesorTitular1.getCodigoProfesor());

        digitalHouseManager.bajaCurso(curso1.getCodigoCurso());


        /*Parte K : ¿Cómo modificaría el diagrama de clases para que se le pueda consultar a un alumno a qué
        cursos está inscripto?

        No estaría bueno que eso suceda, pero en tal caso, el alumno podría tener una lista de los cursos a los cuales está inscriptos.


        */


        // DESTACADO Opción 1:

        LectorDeArchivosCSV lector = new LectorDeArchivosCSV();

        List<Alumno> alumnos = lector.leerAlumnosDeArchivoCSV("listadoDeAlumnos.csv");

        DigitalHouseManager digitalHouseManager2 = new DigitalHouseManager();

        Curso cursoMobileAndroid = new Curso("Mobile Android", 2019, 30);
        digitalHouseManager2.altaCurso(cursoMobileAndroid.getNombre(), cursoMobileAndroid.getCodigoCurso(), cursoMobileAndroid.getCupoMaximo());

        digitalHouseManager2.altaListaAlumnos(alumnos);

        digitalHouseManager2.inscribirListaAlumno(alumnos, 2019);

    }


    private static String imprimirAlumnos(List<Alumno> alumnos) {
        String alumnosConcatenados = "";
        for (int i = 0; i < alumnos.size(); i++) {
            alumnosConcatenados = alumnosConcatenados + " | " + alumnos.get(i).toString();
        }

        return alumnosConcatenados;

    }


}
